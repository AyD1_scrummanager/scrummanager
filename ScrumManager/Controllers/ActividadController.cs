﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ScrumManager.DataBase;
using ScrumManager.Models;

namespace ScrumManager.Controllers
{
    public class ActividadController : Controller
    {
        //
        // GET: /Actividad/

        public ActionResult IndexActividad()
        {
            string usuario = String.Empty;
            //usuario = "1";
            usuario = Session["idUsuario"].ToString();
            SprintModel sprint = new SprintModel();
            sprint.id_usuario = int.Parse(usuario);
            sprint.listaProyectos = new List<ProyectoModel>();
            bdSprint bdSp = new bdSprint();
            bdSp.obtenerProyectos(usuario, sprint);
            return View(sprint);
        }

        public ActionResult verSprint(String proyecto) 
        {
            String resultado = String.Empty;
            bdSprint bdSp = new bdSprint();
            String user = Session["idUsuario"].ToString();
            resultado = bdSp.obtenerSprints(proyecto);
            return Json(new { resultado = resultado });
            
        }

        public ActionResult validarRol(String proyecto)
        {
            String resultado = String.Empty;
            String mensaje = String.Empty;
            bdSprint bdSp = new bdSprint();
            String user = Session["idUsuario"].ToString();
            int rol = bdSp.verRolProyectoAsignado(proyecto, user);
            if (rol == 1)
            {
                resultado = "1";
                mensaje = "Tiene Permisos";
            }

            else if (rol == -1)
            {
                resultado = "-1";
            }
            else
            {
                resultado = "0";
                mensaje = " Usted no tiene permisos para agregar Tareas al Sprint. ";
            }
            return Json(new { resultado = resultado, msj = mensaje });
        }


    }
}
