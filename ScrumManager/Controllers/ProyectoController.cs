﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ScrumManager.DataBase;
using System.Data;
using ScrumManager.Clases;
using ScrumManager.Models;

namespace ScrumManager.Controllers
{
    public class ProyectoController : Controller
    {
        //
        // GET: /Proyecto/

        public ActionResult Index()
        {
            return View();
        
        }

        public ActionResult Proyecto()
        {
            string usuario = String.Empty;
            //usuario = "1";
            usuario = Session["idUsuario"].ToString();
            SprintModel sprint = new SprintModel();
            sprint.id_usuario = int.Parse(usuario);
            sprint.listaProyectos = new List<ProyectoModel>();
            sprint.listaProyectos2 = new List<ProyectoModel>();
            sprint.listaUsuario = new List<UsuarioModel>();
            bdSprint bdSp = new bdSprint();
            bdSp.obtenerUsuarios(sprint);
            bdSp.obtenerProyectos(sprint);
            bdSp.obtenerProyectos(usuario, sprint);
            return View(sprint);
            
        }


        public ActionResult AddProyecto(String proyecto, String fechaIni, String fechaFin)
        {
            /* Respuesta */
            AddProject p = new AddProject();
            string[] respuestas = p.AddPro(proyecto, fechaIni, fechaFin);

            return Json(new { resultado = respuestas[0], msj = respuestas[1] });
        }

        public ActionResult AddAsign(String proyecto, String rol, String id_usuario)
        {
            /* Respuesta */
            AddAsign p = new AddAsign();
            string[] respuestas = p.AddAsignar(proyecto, rol, id_usuario);

            return Json(new { resultado = respuestas[0], msj = respuestas[1] });
        }
    }
}
