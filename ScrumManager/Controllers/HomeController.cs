﻿using System;
using System.Web.Mvc;
using ScrumManager.DataBase;
using System.Data;
using ScrumManager.Clases;
using ScrumManager.Models;

namespace ScrumManager.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/


        public ActionResult Registro()
        {
            return View();
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AcercaDe()
        {
            return View();
        }

        public ActionResult Forms()
        {
            return View();
        }

        public ActionResult Tables()
        {
            return View();
        }

        //Login 
        public ActionResult Login()
        {
            return View();
        }


        //Proyecto 
        public ActionResult Proyecto()
        {
            return View();
        }

        public ActionResult Perfil()
        {
            UsuarioLogueado UsuarioLog = new UsuarioLogueado();
            UserService Consultas = new UserService();
            PerfilModels Usuario = UsuarioLog.GetUsuarioLogueado();
            var Cuenta = Consultas.ObtenerDatosUsuario(Usuario.intIdUsuario);
            Cuenta.Usuario = Usuario;
            return View(Cuenta);
        }

        public ActionResult Ingresar(String user, String pass)
        {
            /* Respuesta */
            String resultado = String.Empty;
            String mensaje = String.Empty;
            UserService login = new UserService();
            DataTable result = login.Login(user, pass);
            if (result.Rows.Count > 0)
            {
                DataRow row = result.Rows[0];
                Session["idUsuario"] = row["id_usuario"].ToString();
                resultado = "1";
                mensaje = "Ok";
            }
            else
            {
                resultado = "0";
                mensaje = "Error";
            }
            return Json(new { resultado = resultado, msj = mensaje });
        }

        public ActionResult Registrar(String user, String pass, String correo, String nombre, String apellido, String direccion)
        {
            /* Respuesta */

            Registro registrar = new Registro();

            string[] respuestas = registrar.registrar(user, pass, correo, nombre, apellido, direccion);

            return Json(new { resultado = respuestas[0], msj = respuestas[1] });
        }
    }
}