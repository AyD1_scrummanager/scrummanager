﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ScrumManager.Models;
using ScrumManager.DataBase;
namespace ScrumManager.Controllers
{
    public class SprintController : Controller
    {
        //
        // GET: /Sprint/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sprint() 
        {
            string usuario = String.Empty;
            //usuario = "1";
             usuario = Session["idUsuario"].ToString(); 
            SprintModel sprint = new SprintModel();
            sprint.id_usuario = int.Parse(usuario);
            sprint.listaProyectos = new List<ProyectoModel>();
            bdSprint bdSp = new bdSprint();
            bdSp.obtenerProyectos(usuario,sprint);
            return View(sprint);
        }

        public ActionResult validarRol(String proyecto)
        {
            String resultado = String.Empty;
            String mensaje = String.Empty;
            bdSprint bdSp = new bdSprint();
            String user = Session["idUsuario"].ToString();
            int rol = bdSp.verRolProyectoAsignado(proyecto,user);
            if (rol == 1)
            {
                resultado = "1";
                mensaje = "Tiene Permisos";
            }

            else if (rol == -1)
            {
                resultado = "-1";
            }
            else 
            {
                resultado = "0";
                mensaje = " Usted no tiene permisos para agregar Sptrint a este proyecto. ";
            }
            return Json(new { resultado = resultado, msj = mensaje });
        }

        public ActionResult NuevoSprint(String nombre, String Inicio, String Fin, String proyecto)
        {
            String resultado = String.Empty;
            String mensaje = String.Empty;
            bdSprint bdSp = new bdSprint();
            String user = Session["idUsuario"].ToString();
            int result = bdSp.agregarSprint(nombre,Inicio,Fin,proyecto);
            if (result == 1)
            {
                resultado = "1";
                mensaje = " Se creo el Nuevo Sprint de Manera Correcta. ";
            }

            else
            {
                resultado = "0";
                mensaje = " No se pudo crear el Sprint, intente de nuevo mas tarde. ";
            }
            return Json(new { resultado = resultado, msj = mensaje });
        }


    }
}
