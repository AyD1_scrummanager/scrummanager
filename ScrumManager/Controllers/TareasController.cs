﻿using ScrumManager.DataBase;
using ScrumManager.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ScrumManager.Controllers
{
    public class TareasController : Controller
    {
        //
        // GET: /Tareas/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult tareasPendientes()
        {
            UserService uServ = new UserService();
            DataTable result = uServ.listaTareas();            
            
            if(result.Rows.Count > 0)
            {
                int intCodTarea = int.Parse(result.Rows[0][0].ToString());
                TareasModels tarea = uServ.tareasPendientes(intCodTarea);
                return View(tarea);
            }
            else
            {
                TareasModels Tarea = new TareasModels
                {
                    intCodTarea = -1,
                    strNombre = "No hay Tareas pendientes",
                    strEstado = "NA",
                };
                return View(Tarea);
            }
            
            
        }

    }
}
