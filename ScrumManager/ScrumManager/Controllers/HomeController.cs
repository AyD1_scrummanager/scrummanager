﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ScrumManager.DataBase;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ScrumManager.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/


        public ActionResult Registro()
        {
            return View();
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AcercaDe()
        {
            return View();
        }

        public ActionResult Forms()
        {
            return View();
        }

        public ActionResult Tables()
        {
            return View();
        }

        //Login 
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Perfil()
        {
            return View();
        }

        public ActionResult Ingresar(String user, String pass)
        {
            /* Respuesta */
            String resultado = String.Empty;
            String mensaje = String.Empty;
            UserService login = new UserService();
            DataTable result = login.Login(user,pass);

            if (result.Rows.Count > 0)
            {
                resultado = "1";
                mensaje = "Ok";
            }
            else 
            {
                resultado = "0";
                mensaje = "Error";
            
            }
            return Json(new { resultado = resultado, msj = mensaje });
        }

        public ActionResult Registrar(String user, String pass, String correo, String nombre, String apellido, String direccion) {
            /* Respuesta */
            String resultado = String.Empty;
            String mensaje = String.Empty;
            UserService registro = new UserService();
            int bandera= registro.Registro(user, pass, correo, nombre, apellido, direccion);

            if (bandera==0)
            {
                resultado = "0";
                mensaje = "El usuario ya Existe...";
            }
            else
            {
                resultado = "1";
                mensaje = "Usuario creado Correctamente...";

            }
            return Json(new { resultado = resultado, msj = mensaje });
        }
    }
}
