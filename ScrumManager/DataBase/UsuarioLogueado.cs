﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ScrumManager.Models;


namespace ScrumManager.DataBase
{
    public class UsuarioLogueado
    {
        private UserService UsuarioObtenido;
        public UsuarioLogueado()
        {
            UsuarioObtenido = new UserService();
        }

        public PerfilModels GetUsuarioLogueado()
        {
            int intIdUsuario = int.Parse(HttpContext.Current.Session["idUsuario"].ToString());
            var usuario = UsuarioObtenido.ObtenerDatosUsuario(intIdUsuario);
            return usuario;
        }
    }
}