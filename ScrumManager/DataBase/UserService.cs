﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using ScrumManager.Models;

namespace ScrumManager.DataBase
{
    public class UserService : Controller
    {
        //
        // GET: /UserService/

        Connection serviceBD;

        public UserService()
        {
            serviceBD = new Connection();
        }

        public DataTable Login(String user, String pass) { 
            Connection conn = new Connection();
            String query = "select * from dbo.Usuario u where u.\"User\"='"+user+"' and u.\"Password\"= '"+pass+"'";
            return conn.FillTableData(query);            
        }

        public DataTable UsuarioExiste(String user)
        {
            Connection conn = new Connection();
            String query = "select * from dbo.Usuario u where u.\"User\"='" + user + "'";
            return conn.FillTableData(query);
        }

        public int Registro(String user, String pass, String correo, String nombre, String apellido, String direccion) 
        {
            Connection conn = new Connection();
            String queryUserExiste = "select * from dbo.Usuario u where u.\"User\"='" + user+"'";
            DataTable result = UsuarioExiste(user);

           //Si el result es mayor a 0 significa que el usuario ya existe 
            if (result.Rows.Count > 0)
            {
                return 0;
            }
            else
            {
                String queryInsertUser = "insert into Usuario values('"+user+"','"+pass+"','"+correo+"','"+nombre+"','"+apellido+"','"+direccion+"',1)";
                conn.FillTableData(queryInsertUser);
                return 1;          
            }
        }

        /*Autor:Roberto Caseros
       * Fecha de modificacion: 1/05/2017
       * Funcion para obtener los datos de un usuario logueado
       * Nombre de la DataBase: SCRUM
       * DBMS: SqlServer 2012
       */
        public PerfilModels ObtenerDatosUsuario(int intUsuario)
        {
            Connection conn = new Connection();
            DataTable Tabla = conn.FillTableData("SELECT A.\"User\",A.Correo,A.Nombre,A.Apellido,A.Direccion FROM Usuario AS A WHERE A.id_usuario =" + intUsuario);
            PerfilModels Usuario = new PerfilModels
            {
                strUsuario = Tabla.Rows[0][0].ToString(),
                strCorreo = Tabla.Rows[0][1].ToString(),
                strNombre = Tabla.Rows[0][2].ToString(),
                strApellido = Tabla.Rows[0][3].ToString(),
                strDireccion = Tabla.Rows[0][4].ToString(),
                intIdUsuario = intUsuario,


            };
            return Usuario;
        }

        /*Autor: Bryan Cordero
        * Fecha de modificacion: 3/05/2017
        * Funcion para obtener el listado de tareas pendientes
        * Nombre de la DataBase: SCRUM
        * DBMS: SqlServer 2012
        */

        public DataTable listaTareas()
        {
            Connection conn = new Connection();
            String query = "select * from Tareas where Estado_id_Estado = 1";
            return conn.FillTableData(query);            
        }

        /*Autor: Bryan Cordero
        * Fecha de modificacion: 3/05/2017
        * Funcion para obtener la tarea pendiente
        * Nombre de la DataBase: SCRUM
        * DBMS: SqlServer 2012
        */
        public TareasModels tareasPendientes(int intNumTarea)
        {
            Connection conn = new Connection();
            DataTable Tabla = conn.FillTableData("SELECT T.cod_Tarea, T.Nombre FROM Tarea T WHERE T.id_usuario =" + intNumTarea + " AND T.Estado_id_Estado = " + 1);
            TareasModels Tarea = new TareasModels
            {
                intCodTarea = int.Parse(Tabla.Rows[0][0].ToString()),
                strNombre = Tabla.Rows[0][1].ToString(),
                strEstado = "Pendiente",
            };
            return Tarea;
        }


        /*Autor:Carlos Gomez
      * Fecha de modificacion: 2/05/2017
      * Funcion para obtener los proyectos de un usuario logueado
      * Nombre de la DataBase: SCRUM
      * DBMS: SqlServer 2012
      */

        public int addPro(String proyecto, String fechaIni, String fechaFin) {

            Connection conn = new Connection();
            String queryInsertUser = "insert into Proyecto values('" + proyecto + "','" + fechaIni + "','" + fechaFin + "')";
            conn.FillTableData(queryInsertUser);
            return 1;
           
        }

        public int addAsignar(String proyecto, String rol, String id_usuario)
        {

            Connection conn = new Connection();
            String queryInsertUser = "insert into Asignacion_Proyecto values('" + id_usuario + "','" + rol + "','" + proyecto + "')";
            conn.FillTableData(queryInsertUser);
            return 1;

        } 
    


    }
}
