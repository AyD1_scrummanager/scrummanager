﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using ScrumManager.Models;

namespace ScrumManager.DataBase
{
    public class bdSprint
    {
        Connection serviceBD;

        public bdSprint()
        {
            serviceBD = new Connection();
        }

        public String obtenerSprints(String idProyecto) 
        {
            String resultado = "";
            resultado = "<select id=\"selectScript\">" + "<option value=\"-1\"> Selecione Un Script</option>";
            String query = "SELECT * from dbo.Sprint S WHERE S.Proyecto_id_Proyecto =" + idProyecto;
            DataTable tbAsig = serviceBD.FillTableData(query);
            if (tbAsig.Rows.Count > 0)
            {
                foreach (DataRow row in tbAsig.Rows)
                {
                    String idSprint = row["id_Sprint"].ToString();
                    String Nombre = row["Nombre"].ToString();
                    resultado += "<option value= \""+ idSprint+"\"> "+ Nombre +"</option>";
                }
            }
            resultado += "</select>";

            return resultado;
        }
        public void obtenerProyectos(String idUsuario,SprintModel sprint) 
        {

            String query = "SELECT * from dbo.Asignacion_Proyecto AP WHERE AP.Usuario_id_usuario =" + idUsuario ;
            DataTable tbAsig =  serviceBD.FillTableData(query); 
            if(tbAsig.Rows.Count > 0)
            {
                foreach(DataRow row in tbAsig.Rows)
                {
                    String idProyecto = row["Proyecto_id_proyecto"].ToString();
                    ProyectoModel proyecto = obtenerProyecto(idProyecto);
                    sprint.listaProyectos.Add(proyecto);
                    sprint.listaProyectos2.Add(proyecto);
                }
            }
        }

        public void obtenerProyectos(SprintModel sprint)
        {

            String query = "SELECT * from dbo.proyecto";
            DataTable tbAsig = serviceBD.FillTableData(query);
            if (tbAsig.Rows.Count > 0)
            {
                foreach (DataRow row in tbAsig.Rows)
                {
                    ProyectoModel proyecto = obtenerProyecto(row);
                    sprint.listaProyectos.Add(proyecto);
                }
            }
        }

        public void obtenerUsuarios(SprintModel sprint)
        {

            String query = "SELECT * from dbo.Usuario";
            DataTable tbAsig = serviceBD.FillTableData(query);
            if (tbAsig.Rows.Count > 0)
            {
                foreach (DataRow row in tbAsig.Rows)
                {
                    UsuarioModel us = obtenerUser(row);
                    sprint.listaUsuario.Add(us);
                }
            }
        }

        public UsuarioModel obtenerUser(DataRow row) {
            UsuarioModel usuario = new UsuarioModel();
                    usuario.id_usuario = int.Parse(row["id_usuario"].ToString());
                    usuario.User = row["User"].ToString();
                    usuario.Password = row["Password"].ToString();
                    usuario.Correo = row["Correo"].ToString();
                    usuario.Nombre = row["Nombre"].ToString();
                    usuario.Apellido = row["Apellido"].ToString();
                    usuario.Direccion = row["Direccion"].ToString();
                    usuario.TipoUsuario_cod_tipo = int.Parse(row["TipoUsuario_cod_tipo"].ToString());
            return usuario;
        }

        public ProyectoModel obtenerProyecto(String idProyecto) 
        {
            ProyectoModel proyecto = new ProyectoModel();
            String query = "SELECT * from dbo.Proyecto P WHERE P.id_Proyecto =" + idProyecto;
            DataTable dt = serviceBD.FillTableData(query);
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                proyecto.id_proyecto = int.Parse(row["id_Proyecto"].ToString());
                proyecto.nombreProyecto = row["Nombre"].ToString();
                proyecto.fechaInicio = DateTime.Parse(row["Fecha_Inicio"].ToString());
                proyecto.fechaFin = DateTime.Parse(row["Fecha_Final"].ToString());
                
            }
            return proyecto;
        }


        public ProyectoModel obtenerProyecto(DataRow row)
        {
                ProyectoModel proyecto = new ProyectoModel();
                proyecto.id_proyecto = int.Parse(row["id_Proyecto"].ToString());
                proyecto.nombreProyecto = row["Nombre"].ToString();
                proyecto.fechaInicio = DateTime.Parse(row["Fecha_Inicio"].ToString());
                proyecto.fechaFin = DateTime.Parse(row["Fecha_Final"].ToString());

            return proyecto;
        }

        public int verRolProyectoAsignado(String idProyecto, String idUsuario)
        {
            int rol = -1;
            String query = "SELECT AP.Rol_cod_Rol as Rol from dbo.Asignacion_Proyecto AP WHERE AP.Proyecto_id_Proyecto =" + idProyecto + " AND " + " AP.Usuario_id_usuario = " + idUsuario;
            DataTable dt = serviceBD.FillTableData(query);
            if (dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];
                rol = int.Parse(row["Rol"].ToString());
            }
            return rol;
        }
        public int agregarSprint(String nombre, String Inicio,String Fin, String proyecto) 
        {
            int result = 1;
            String campos = "INSERT INTO dbo.SPRINT(Nombre,Fecha_Inicio,Fecha_Fin,Proyecto_id_Proyecto)";
            String valores = string.Format(" VALUES('{0}','{1}','{2}',{3})", nombre, Inicio, Fin, proyecto);
            String query = campos + valores;
            DataTable dt = serviceBD.FillTableData(query);
            return result;
        }
    }
}