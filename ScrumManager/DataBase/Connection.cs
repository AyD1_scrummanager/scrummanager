﻿/*
 Clase Connection
 *  --Autor: Carlos Gómez
 *  -- Clase con metodos para realizar la conexion hacia la base de Datos SQL Server
 *  -- y metodos relacionados a la base de datos
 *  -- Fecha de modificacion: 28/03/2017
 * 
 * NOTA: SE AGREGO LA REFERENCIA A System.Windows.Forms
 *      Para agregar referencia...
 *      --Add -> references -> Assemblies -> Framework -> System.Windows.Froms 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
/*Se añade la libreria SqlClient para realizar la conexion de la base de datos
y demas metodos relacionados a la base de Datos*/
using System.Windows.Forms;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace ScrumManager.DataBase
{
    public class Connection : Controller
    {
        //
        // GET: /Connection/

        public ActionResult Index()
        {
            return View();
        }

        public SqlConnection connection;

        //String de Conexion
        public void Initialize()
        {

            string connectionString = ConfigurationManager.ConnectionStrings["SQLSTR"].ConnectionString;
            connection = new SqlConnection(connectionString);

        }

        //Abre conexion a la base
        public bool OpenConnection()
        {
            Initialize();
            try
            {
                connection.Open();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }
        /*Autor:Carlos Gomez
         * Fecha de modificacion: 28/03/2017
         * Metodo para conectar la base de Datos con la aplicación
         * Nombre de la DataBase: SCRUM
         * DBMS: SqlServer 2012
         */
        public void Conectar() {
            SqlConnection conexion = new SqlConnection("server=CARLOS_GOMEZ ; database=SCRUM ; integrated security = true");
            conexion.Open();
            MessageBox.Show("Se abrió la conexión con el servidor SQL Server");
            conexion.Close();
            MessageBox.Show("Se cerró la conexión....");
        }


        //Obtiene tabla 
        public DataTable FillTableData(string pQuery)
        {
            DataTable DtResultado = new DataTable();

            try
            {
                SqlDataAdapter cmd = new SqlDataAdapter(pQuery, connection);
                cmd.Fill(DtResultado);
            }
            catch
            {
                Initialize();
                SqlDataAdapter cmd = new SqlDataAdapter(pQuery, connection);
                try
                {
                    cmd.Fill(DtResultado);
                }
                catch (Exception ex)
                {
                    //HttpContext.Current.Session["Error"] = ex.Message;
                }

            }
            return DtResultado;
        }



        //Ejecutar Query
        public void ExecQuery(string pQuery)
        {
            
            try
            {

                SqlCommand cmd = new SqlCommand(pQuery, connection);
                cmd.ExecuteNonQuery();
            }
            catch
            {
                Initialize();
                SqlCommand cmd = new SqlCommand(pQuery, connection);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //HttpContext.Current.Session["Error"] = ex.Message;
                }

            }
            
        }
        

        //Verifica conexion
        public string TestConexion()
        {
            bool Test = OpenConnection();
            string resultado;
            if (Test == true)
            { resultado = "Great";
            MessageBox.Show("CONEXION ESTABLECIDA....");
            }
            else
            { resultado = "Conexion Fallida";
            MessageBox.Show("CONEXION FALLIDA...");
            }

            return resultado;
        }

    }
}
