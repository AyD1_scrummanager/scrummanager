-- Generado por Oracle SQL Developer Data Modeler 4.1.3.901
--   en:        2017-03-07 17:31:14 CST
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g




CREATE DATABASE SCRUM;
USE SCRUM;



CREATE TABLE Asignacion_Proyecto
  (
    Usuario_id_usuario   INTEGER NOT NULL ,
    Rol_cod_Rol          INTEGER NOT NULL ,
    Proyecto_id_Proyecto INTEGER NOT NULL
  ) ;
ALTER TABLE Asignacion_Proyecto ADD CONSTRAINT AP_PK PRIMARY KEY ( Usuario_id_usuario, Proyecto_id_Proyecto ) ;


CREATE TABLE Asignacion_Tarea
  (
    Asunto             VARCHAR (50) NOT NULL ,
    Usuario_id_usuario INTEGER NOT NULL ,
    Tarea_cod_Tarea    INTEGER NOT NULL
  ) ;
ALTER TABLE Asignacion_Tarea ADD CONSTRAINT Asignacion_Tarea_PK PRIMARY KEY ( Usuario_id_usuario, Tarea_cod_Tarea ) ;


CREATE TABLE Backlog
  (
    id_backlog           INTEGER IDENTITY(1,1) NOT NULL ,
    Nombre               VARCHAR (50) NOT NULL ,
    Proyecto_id_Proyecto INTEGER NOT NULL
  ) ;
ALTER TABLE Backlog ADD CONSTRAINT Backlog_PK PRIMARY KEY ( id_backlog ) ;


CREATE TABLE Estado
  (
    id_Estado INTEGER IDENTITY(1,1) NOT NULL ,
    Nombre    VARCHAR (25) NOT NULL
  ) ;
ALTER TABLE Estado ADD CONSTRAINT Estado_PK PRIMARY KEY ( id_Estado ) ;


CREATE TABLE Proyecto
  (
    id_Proyecto  INTEGER IDENTITY(1,1) NOT NULL ,
    Nombre       VARCHAR (50) NOT NULL ,
    Fecha_Inicio DATE NOT NULL ,
    Fecha_Final  DATE NOT NULL
  ) ;
ALTER TABLE Proyecto ADD CONSTRAINT Proyecto_PK PRIMARY KEY ( id_Proyecto ) ;


CREATE TABLE Rol
  (
    cod_Rol INTEGER IDENTITY(1,1) NOT NULL ,
    Nombre  VARCHAR (25) NOT NULL
  ) ;
ALTER TABLE Rol ADD CONSTRAINT Rol_PK PRIMARY KEY ( cod_Rol ) ;


CREATE TABLE Sprint
  (
    id_Sprint            INTEGER IDENTITY(1,1) NOT NULL ,
    Nombre               VARCHAR (50) NOT NULL ,
    Fecha_Inicio         DATE NOT NULL ,
    Fecha_Fin            DATE NOT NULL ,
    Proyecto_id_Proyecto INTEGER NOT NULL
  ) ;
ALTER TABLE Sprint ADD CONSTRAINT Sprint_PK PRIMARY KEY ( id_Sprint ) ;


CREATE TABLE Tarea
  (
    cod_Tarea          INTEGER IDENTITY(1,1) NOT NULL ,
    Nombre             VARCHAR (50) NOT NULL ,
    Descripcion        VARCHAR (150) NOT NULL ,
    Tiempo_Estimado    DATE NOT NULL ,
    Tiempo_Dedicado    DATE ,
    Estado_id_Estado   INTEGER NOT NULL ,
    Sprint_id_Sprint   INTEGER NOT NULL ,
    Backlog_id_backlog INTEGER NOT NULL
  ) ;
ALTER TABLE Tarea ADD CONSTRAINT Tarea_PK PRIMARY KEY ( cod_Tarea ) ;


CREATE TABLE Tiempo_Dedicado
  (
    Descripcion        VARCHAR (150) NOT NULL ,
    Tiempo             DATE NOT NULL ,
    Fecha              DATE NOT NULL ,
    Usuario_id_usuario INTEGER NOT NULL ,
    Tarea_cod_Tarea    INTEGER NOT NULL
  ) ;


CREATE TABLE TipoUsuario
  (
    cod_tipo INTEGER IDENTITY(1,1) NOT NULL ,
    Nombre   VARCHAR (25) NOT NULL
  ) ;
ALTER TABLE TipoUsuario ADD CONSTRAINT TipoUsuario_PK PRIMARY KEY ( cod_tipo ) ;


CREATE TABLE Usuario
  (
    id_usuario           INTEGER IDENTITY(1,1) NOT NULL ,
    "User"               VARCHAR (50) NOT NULL ,
    Password             VARCHAR (25) NOT NULL ,
    Correo               VARCHAR (50) NOT NULL ,
    Nombre               VARCHAR (25) NOT NULL ,
    Apellido             VARCHAR (25) NOT NULL ,
    Direccion            VARCHAR (100) NOT NULL ,
    TipoUsuario_cod_tipo INTEGER NOT NULL
  ) ;
ALTER TABLE Usuario ADD CONSTRAINT Usuario_PK PRIMARY KEY ( id_usuario ) ;


ALTER TABLE Asignacion_Proyecto ADD CONSTRAINT AP_P_FK FOREIGN KEY ( Proyecto_id_Proyecto ) REFERENCES Proyecto ( id_Proyecto ) ;

ALTER TABLE Asignacion_Proyecto ADD CONSTRAINT AP_R_FK FOREIGN KEY ( Rol_cod_Rol ) REFERENCES Rol ( cod_Rol ) ;

ALTER TABLE Asignacion_Proyecto ADD CONSTRAINT AP_U_FK FOREIGN KEY ( Usuario_id_usuario ) REFERENCES Usuario ( id_usuario ) ;

ALTER TABLE Asignacion_Tarea ADD CONSTRAINT Asignacion_Tarea_Tarea_FK FOREIGN KEY ( Tarea_cod_Tarea ) REFERENCES Tarea ( cod_Tarea ) ;

ALTER TABLE Asignacion_Tarea ADD CONSTRAINT Asignacion_Tarea_Usuario_FK FOREIGN KEY ( Usuario_id_usuario ) REFERENCES Usuario ( id_usuario ) ;

ALTER TABLE Backlog ADD CONSTRAINT Backlog_Proyecto_FK FOREIGN KEY ( Proyecto_id_Proyecto ) REFERENCES Proyecto ( id_Proyecto ) ;

ALTER TABLE Sprint ADD CONSTRAINT Sprint_Proyecto_FK FOREIGN KEY ( Proyecto_id_Proyecto ) REFERENCES Proyecto ( id_Proyecto ) ;

ALTER TABLE Tarea ADD CONSTRAINT Tarea_Backlog_FK FOREIGN KEY ( Backlog_id_backlog ) REFERENCES Backlog ( id_backlog ) ;

ALTER TABLE Tarea ADD CONSTRAINT Tarea_Estado_FK FOREIGN KEY ( Estado_id_Estado ) REFERENCES Estado ( id_Estado ) ;

ALTER TABLE Tarea ADD CONSTRAINT Tarea_Sprint_FK FOREIGN KEY ( Sprint_id_Sprint ) REFERENCES Sprint ( id_Sprint ) ;

ALTER TABLE Tiempo_Dedicado ADD CONSTRAINT Tiempo_Dedicado_Tarea_FK FOREIGN KEY ( Tarea_cod_Tarea ) REFERENCES Tarea ( cod_Tarea ) ;

ALTER TABLE Tiempo_Dedicado ADD CONSTRAINT Tiempo_Dedicado_Usuario_FK FOREIGN KEY ( Usuario_id_usuario ) REFERENCES Usuario ( id_usuario ) ;

ALTER TABLE Usuario ADD CONSTRAINT Usuario_TipoUsuario_FK FOREIGN KEY ( TipoUsuario_cod_tipo ) REFERENCES TipoUsuario ( cod_tipo ) ;




-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            11
-- CREATE INDEX                             0
-- ALTER TABLE                             23
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
