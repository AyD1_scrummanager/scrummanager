﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ScrumManager.DataBase;

namespace ScrumManager.Clases
{
    public class AddAsign
    {
        public string[] AddAsignar(String proyecto, String rol, String id_usuario)
        {
            String resultado = String.Empty;
            String mensaje = String.Empty;
            UserService p = new UserService();
            p.addAsignar(proyecto, rol, id_usuario);

            resultado = "1";
            mensaje = "Proyecto agregado ";


            return new string[] { resultado, mensaje };
        }
    }
}