﻿using ScrumManager.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScrumManager.Clases
{
    public class Registro
    {

        public string[] registrar(String user, String pass, String correo, String nombre, String apellido, String direccion)
        {
            String resultado = String.Empty;
            String mensaje = String.Empty;
            UserService registro = new UserService();
            int bandera = registro.Registro(user, pass, correo, nombre, apellido, direccion);

            if (bandera == 0)
            {
                resultado = "0";
                mensaje = "El usuario ya Existe...";
            }
            else
            {
                resultado = "1";
                mensaje = "Usuario creado Correctamente...";

            }

            return new string[] {resultado,mensaje };
        }

    }
}