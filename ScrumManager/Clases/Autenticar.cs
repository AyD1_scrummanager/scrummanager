﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ScrumManager.DataBase;
using System.Data;
using ScrumManager.Clases;
using ScrumManager.Models;

namespace ScrumManager.Clases
{
    public class Autenticar
    {
        public string[] Login(String user, String pass)
        {
            String resultado = String.Empty;
            String mensaje = String.Empty;
            UserService login = new UserService();
            DataTable result = login.Login(user, pass);

            if (result.Rows.Count > 0)
            {
                DataRow row = result.Rows[0];
                //Session["idUsuario"] = row["id_usuario"].ToString();
                resultado = "1";
                mensaje = "Ok";
            }
            else
            {
                resultado = "0";
                mensaje = "Error";

            }
            return new string[] { resultado, mensaje };
        }
    }
}