﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScrumManager.Models
{
    public class TareasModels
    {
        public int intCodTarea { get; set; }
        public string strNombre { get; set; }
        public string strEstado { get; set; }

        public TareasModels Tarea { get; set; }
    }
}