﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScrumManager.Models
{
    public class UsuarioModel
    {
        public int id_usuario { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Correo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public int TipoUsuario_cod_tipo { get; set; }

        public List<UsuarioModel> listaUsuario;

        
    }
}