﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScrumManager.Models
{
    public class ProyectoModel
    {
        public int id_proyecto { get; set; }
        public string nombreProyecto { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
    }
}