﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScrumManager.Models
{
    public class PerfilModels
    {
        public int intIdUsuario { get; set; }
        public string strUsuario { get; set; }
        public string strCorreo { get; set; }
        public string strNombre { get; set; }
        public string strApellido { get; set; }
        public string strDireccion { get; set; }
        
        public PerfilModels Usuario { get; set; }

    }
}